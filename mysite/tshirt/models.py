from __future__ import unicode_literals
from django.utils.encoding import smart_unicode
from django.core.urlresolvers import reverse

from django.db import models


class VendorPost(models.Model):
    id = models.IntegerField(primary_key=True)
    text =models.CharField(max_length =500)
    data = models.CharField(max_length =100)
    picture = models.ImageField(upload_to='tshirt')

    def __unicode__(self):
        return smart_unicode(self.id)
