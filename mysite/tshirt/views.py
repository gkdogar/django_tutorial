from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .forms import ShirtForm

# Create your views here.
from.models import VendorPost
class IndexView(generic.ListView):
    form=ShirtForm()
    context={'myregistrationform':form}
    template_name='tshirt/form.html'
    context_object_name='all_data'
    def get_queryset(self):
        return VendorPost.objects.all()

class DetailView(generic.DetailView):
    model=VendorPost
    template_name='tshirt/detail.html'
