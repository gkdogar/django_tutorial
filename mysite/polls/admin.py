from django.contrib import admin
from .models import Question
# Register your models here.
admin.site.register(Question)
def was_published_recently(self):
    now = timezone.now()
    return now - datetime.timedelta(days=1) <= self.pub_date <= now
