from django.db import models
from django.forms import forms
import os
# Create your models here.

class VendorPost(models.Model):
    id = models.AutoField(primary_key=True)
    text =models.CharField(max_length =500)
    data = models.CharField(max_length =100)
    picture = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.text
