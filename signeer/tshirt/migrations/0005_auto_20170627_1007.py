# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-27 10:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tshirt', '0004_auto_20170626_2312'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vendorpost',
            name='picture',
            field=models.ImageField(upload_to=b'media/'),
        ),
    ]
