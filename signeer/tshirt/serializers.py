from rest_framework import serializers
from .models import VendorPost

class VendorPost_Serializer(serializers.ModelSerializer):

    class Meta:
        model = VendorPost
        #fields =('text', 'data', 'picture')
        #fields =''__all__'
        fields = ('picture')
