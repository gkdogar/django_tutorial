from django.forms import ModelForm
from .models import VendorPost



class TshirtForm(ModelForm):

    class Meta:
        model =VendorPost
        fields=('id','text','data','picture')
    
