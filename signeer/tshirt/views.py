from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from django.shortcuts import get_list_or_404
from rest_framework.views import APIView
from rest_framework.response import  responses
from rest_framework import status
from django.conf import settings



from.models import VendorPost
from.forms import TshirtForm
from . serializers import VendorPost_Serializer
# Create your views here.


def upload_file (request):

    #posts = VendorPost.objects.all()
    form=TshirtForm(request.POST, request.FILES)
    if form.is_valid():
        instance=form.save(commit=False)
        instance.save()
        print form.cleaned_data.get("text")


    #if request.method=="POST":
        #print request.POST.get("text")
        #print request.POST.get("data")
        #print request.POST.get("picture")
        #print request.POST.get("id")
    context ={'form':form}
    return render(request,'tshirt/upload_file.html', context)


def detail(request):
    #data_form=TshirtForm()
    data_form=VendorPost.objects.all()
    return render(request,'tshirt/detail.html',{'data_form': data_form, 'media_url': settings.MEDIA_URL})
   #<!-------------------NOW Start RESTFRAMEWORK---------------------------->



# serilization/
def  post_serilization(APIView):

    def get(self, request,format=None):
        vendor_obj =VendorPost.objects.all()
        serializers =VendorPost_Serializer(vendor_obj, many =True)
        return responses(serializers.data)
