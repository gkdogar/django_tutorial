from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from .import views



app_name='tshirt'
urlpatterns = [
    url(r'^$', views.detail, name='detail'),

   #url(r'^rest/$', views.rest_query, name='rest_query'),
    url(r'^upload_file', views.upload_file, name='upload_file'),

    #stocks/
    url(r'^serilization/', views.post_serilization, name= 'serilization')

]
urlpatterns = format_suffix_patterns(urlpatterns)
